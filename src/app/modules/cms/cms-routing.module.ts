import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CmsComponent } from './cms.component';
import { UsersComponent } from './pages/users/users.component';
import { DashboardsComponent } from './pages/dashboards/dashboards.component';
import { LoginComponent } from './pages/login/login.component';

import { ListQuestionComponent } from './pages/list-question/list-question.component';

const routes: Routes = [
  {
    path: 'admin',
    redirectTo: '/admin/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'admin/login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: CmsComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardsComponent
      },
      {
        path: 'user',
        component: UsersComponent
      },
      {
        path: 'list-question',
        component: ListQuestionComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
