import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CmsRoutingModule } from './cms-routing.module';
import { UsersComponent } from './pages/users/users.component';
import { CmsComponent } from './cms.component';
import { DashboardsComponent } from './pages/dashboards/dashboards.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ListQuestionComponent } from './pages/list-question/list-question.component';
import { LoginComponent } from './pages/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { from } from 'rxjs';

@NgModule({
  declarations: 
  [
    CmsComponent,
     UsersComponent, 
     DashboardsComponent, 
     HeaderComponent, 
     SidebarComponent,
     ListQuestionComponent,
     LoginComponent
    ],


  imports: [
    CommonModule,
    CmsRoutingModule,
    FormsModule,
    ReactiveFormsModule 
  ]
})
export class CmsModule { }
