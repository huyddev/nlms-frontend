import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormArray, FormBuilder} from '@angular/forms';
import { SelectorListContext } from '@angular/compiler';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {
  formAdd:any;
  listanswer : any = [];

  answer: {
    content: string,
    selling_points: []
  }

  SellingPoint: {
    selling_point: string,
    istrue: boolean
  }
  constructor(private fb: FormBuilder) {
    this.formAdd = new FormGroup({
      questionContent: new FormControl(''),
      url: new FormControl(''),
      lessionID: new FormControl(''),
      difficulty: new FormControl(''),
      time: new FormControl(''),
      questionType: new FormControl(''),
      answercontent: new FormControl(''),
      istrue: new FormControl('')
    })
   }

  ngOnInit() {
    this.formAdd = this.fb.group({
      selling_points: this.fb.array([this.fb.group({answercontent:'', istrue:''})])
    })
  }
  // get sellingPoints(){
  //   return this.formAdd.get('selling_points') as FormArray;
  // }

  // addSellingPoint() {
  //   this.sellingPoints.push(this.fb.group({point:''}));
  // }

  // deleteSellingPoint(index) {
  //   this.sellingPoints.removeAt(index);
  // }
  addQuestion(){
    this.listanswer.push(this.formAdd.value.answercontent, this.formAdd.value.istrue);
    console.log(this.listanswer);
    console.log(this.formAdd.value);
  }
}
