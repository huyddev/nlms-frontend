import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { TestingComponent } from './pages/testing/testing.component';
import { TestResultsComponent } from './pages/test-results/test-results.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SlidebarComponent } from './components/slidebar/slidebar.component';
import { UserTestManagerComponent } from './pages/user-test-manager/user-test-manager.component';
import { CountdownModule } from 'ngx-countdown';

import { ListQuestionComponent } from './pages/list-question/list-question.component';
import { UserComponent } from './pages/user/user.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
@NgModule({
  declarations: [
    HomeComponent, 
    TestingComponent, 
    TestResultsComponent, 
    FooterComponent, 
    HeaderComponent, 
    SlidebarComponent, 
    UserTestManagerComponent, 
    ListQuestionComponent, 
    UserComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CountdownModule,
    FormsModule,
    ReactiveFormsModule
  ]

})
export class HomeModule { }
