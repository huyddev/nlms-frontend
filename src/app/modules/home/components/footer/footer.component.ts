import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  divhien:number=0;
  constructor() { }

  ngOnInit() {
  }
  hien():void{
    this.divhien=1;
  }
  an():void{
    this.divhien=0;
  }
}
