import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagerSlidebarComponent } from './user-manager-slidebar.component';

describe('UserManagerSlidebarComponent', () => {
  let component: UserManagerSlidebarComponent;
  let fixture: ComponentFixture<UserManagerSlidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserManagerSlidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagerSlidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
