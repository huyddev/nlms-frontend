import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { TestingComponent } from './pages/testing/testing.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';

import { UserTestManagerComponent } from './pages/user-test-manager/user-test-manager.component';
import { ListQuestionComponent } from './pages/list-question/list-question.component';
import { UserComponent } from './pages/user/user.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'testing',
        component: TestingComponent
      },
      {
        path: 'userM',
        component: UserTestManagerComponent
      },
     
      {
        path: 'updateQuestion',
        component: ListQuestionComponent
      },
      {
        path: 'user',
        component: UserComponent
      },
      {
        path: 'login',
        component: LoginComponent        
      },
      {
        path: 'register',
        component: RegisterComponent
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
