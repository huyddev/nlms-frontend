import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {
  timeLeft: number = 900000;
  formatSecond: number;
  formatMinute: number;
  formatHour: number;
  subscribeTimer: any;
  interval: any;


  //anh Phương
  percent: any = 13;
  tallquestion: any = 31;
  percentCorect: any = Math.round((this.percent / this.tallquestion * 100));
  level: any;
  //end anh Phương

  questionArray: any = [
    { id: 1, questionContent: 'Phương thức viết chương trình của javascript ntn?1', answerContent: [{ id: 11, answer: 'a1' },{ id: 111, answer: 'b1' }, { id: 1111, answer: 'b3' }, { id: 11111, answer: 'b4' }], answerType: 'radio' },
    { id: 2, questionContent: 'Phương thức viết chương trình của javascript ntn?2', answerContent: [{ id: 22, answer: 'a1' },{ id: 222, answer: 'b1' }, { id: 2222, answer: 'b3' }, { id: 22222, answer: 'b4' }], answerType: 'radio' },
    { id: 3, questionContent: 'Phương thức viết chương trình của javascript ntn?3', answerContent: [{ id: 33, answer: 'a1' },{ id: 333, answer: 'b1' }, { id: 3333, answer: 'b3' }, { id: 33333, answer: 'b4' }], answerType: 'checkbox' },
    // { id: 4, questionContent: 'Phương thức viết chương trình của javascript ntn?4', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'checkbox' },
    // { id: 5, questionContent: 'Phương thức viết chương trình của javascript ntn5aaaaaaaa', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'radio' },
    // { id: 6, questionContent: 'Phương thức viết chương trình của javascript ntn?6', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'checkbox' },
    // { id: 7, questionContent: 'Phương thức viết chương trình của javascript ntn?7', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'radio' },
    // { id: 8, questionContent: 'Phương thức viết chương trình của javascript ntn?8', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'checkbox' },
    // { id: 9, questionContent: 'Phương thức viết chương trình của javascript ntn?9', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'radio' },
    // { id: 10, questionContent: 'Phương thức viết chương trình của javascript ntn?10', answerContent: [{ answer: 'a1' },{ answer: 'b1' }, { answer: 'b3' }, { answer: 'b4' }], answerType: 'radio' }
  ];

  questionQuantity: any = []
  tempArray = []
  answerArray: any = [];
  i: number = 0;
  j: number = 1;
  checkbtnNext: number = 0;
  checkbtnPrevious: number = 0;
  count: number;
  currentQuestion: any = this.questionArray[0];
  theCheckbox = false;
  constructor() { }

  ngOnInit() {
    this.count = 0;
    this.getquestionQuantity()
    this.checkbtnPrevious = 0;
  }

  getquestionQuantity() {

    for (let i = 1; i <= this.questionArray.length; i++) {
      this.questionQuantity.push(i);
    }
    console.log('i phan tu', this.questionQuantity)
  }

  nextButton() {
    let _indexQuestion = this.questionArray.map(q => q.id).indexOf(this.currentQuestion.id);
    this.currentQuestion = this.questionArray[_indexQuestion + 1];
    this.checkbtnPrevious = 1;
    if (_indexQuestion + 1 == this.questionArray.length - 1) {
      this.checkbtnNext = 0;
    }
    //   this.count++;
    //   this.checkbtnPrevious = 0;
    // if (this.count == this.questionArray.length - 1) {
    //   this.checkbtnNext = 0;
    // }
  }

  previousButton() {
    let _indexQuestion = this.questionArray.map(q => q.id).indexOf(this.currentQuestion.id);
    this.currentQuestion = this.questionArray[_indexQuestion - 1];
    // this.count--;
    this.checkbtnNext = 1;
    if (_indexQuestion == 1) {
      this.checkbtnPrevious = 0;
    }
  }

  leftQuestionList(pos: any) {
    this.currentQuestion = this.questionArray[pos];
    // this.count = pos;
    if ((pos + 1) == this.questionArray.length) {
      this.checkbtnNext = 0;
      this.checkbtnPrevious = 1;
    }
    else if ((pos) == 0) {
      this.checkbtnPrevious = 0;
      this.checkbtnNext = 1;
    }
    else {
      this.checkbtnNext = 1;
      this.checkbtnPrevious = 1;
    }

  }

  leveltest() {
    if (this.percentCorect >= 75) {
      this.level = "Great";
    } else {
      if (this.percentCorect > 50 && this.percentCorect < 75) { this.level = "Medium"; }
      else { this.level = "Weak" }
    }
  }

  _selectAnswer(asw: any) {
    console.log("_selectAnswer", asw)
    let _indexQuestion = this.questionArray.map(q => q.id).indexOf(this.currentQuestion.id);
    console.log("_indexQuestion", _indexQuestion)
    let _indexAnswer = this.questionArray[_indexQuestion].answerContent.map(a => a.id).indexOf(asw.id);
    console.log("_indexAnswer", _indexAnswer)
    console.log("questionArray", this.questionArray)
    this.questionArray[_indexQuestion].answerContent[_indexAnswer].checked = true;
    console.log("end questionArray", this.questionArray)
    this.changeColor(asw);
  }

  changeColor(a: any) {
    a = this.count;
    console.log('aa', a)
    let x = document.getElementById(a);
    x.style.backgroundColor = 'blue';
  }


}
