import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginServiceService } from 'src/app/shared/services/login-service.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public user: any = {
    fullname: '',
    userName: '',
    email: '',
    password: '',
  }
  
  userForm: any;
  show= false;
  pass: any;
  repass: any;

  verifiPass = false;
  
  constructor( private _service: LoginServiceService, private _router: Router) { 
    this.userForm = new FormGroup ({
      fullname: new FormControl('', [
        Validators.required, 
        // Validators.minLength(8), 
        // Validators.maxLength(32)
      ]),
      userName: new FormControl('',[
        Validators.required
      ]),
      email: new FormControl('',[
        Validators.required,
        // Validators.minLength(8),
        // Validators.maxLength(32),
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)      
        
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      repassword: new FormControl('')
    });
  }

  ngOnInit() {
    
  }

  checkPass(event: any){    
    console.log(this.pass)
    console.log(this.repass)
    if(this.pass == this.repass){
      this.verifiPass = true;
    }
    else{
      this.verifiPass = false;
    }
  }

  onRegister(){
    if(this.userForm.valid && this.verifiPass==true){
      this.user = this.userForm.value;
      console.log(this.user);
      // this._router.navigateByUrl('login');
    }
    else{
      return;
    }

    
  }

}
