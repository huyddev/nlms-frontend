import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {
  formAdd:any;
  constructor() {
    this.formAdd = new FormGroup({
      questionContent: new FormControl(''),
      url: new FormControl(''),
      lessionID: new FormControl(''),
      difficulty: new FormControl(''),
      time: new FormControl(''),
      questionType: new FormControl(''),
      answercontent: new FormControl(''),
      istrue: new FormControl('')
    })
   }

  ngOnInit() {
  }
  addQuestion(){
    console.log(this.formAdd.value);
  }
}