import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTestManagerComponent } from './user-test-manager.component';

describe('UserTestManagerComponent', () => {
  let component: UserTestManagerComponent;
  let fixture: ComponentFixture<UserTestManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTestManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTestManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
