import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator } from '@angular/forms';
import { from } from 'rxjs';
import { LoginServiceService } from 'src/app/shared/services/login-service.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: any = {
    userName: '',
    email: '',
    password: '',
  }
  
  userLogin: any;
  constructor(private _service: LoginServiceService, private _router: Router) {
    this.userLogin = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        // Validators.minLength(4),
        // Validators.maxLength(20),
        // Validators.email,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)     
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
   }

  ngOnInit() {
  }

  onLogin(){
    if(this.userLogin.invalid){
      return;
    }
    else{
      this.user = this.userLogin.value;
      // this._service.postUserLogin(this.user).subscribe(data => console.log("data",data))
      console.log(this.user);
      // this._router.navigateByUrl('');
    }
  }
}
