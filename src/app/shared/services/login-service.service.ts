import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  
  httpOptions: any;
  constructor(private http: HttpClient) { 
    this.httpOptions = {
      headers: new HttpHeaders(
        { 'Content-Type': 'application/json' })
    }
  }
  postUserLogin(user: any){
    return this.http.post('http://10.124.170.43:1997/api/login/', user, this.httpOptions);
  }
  postUserRegister(){
    console.log("da vao");
    // return this.http.post<any>('http://192.168.10.114:1997/api/register', this.user, this.httpOptions);
  }
}
